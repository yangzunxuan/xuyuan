//
//  AppDelegate.h
//  ServerRequestTest
//
//  Created by cocoa on 13-10-18.
//  Copyright (c) 2013年 cocoajin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
